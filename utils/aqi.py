AIR_POLLUTION = {
    "fantastic": "优",
    "good": "良",
    "moderate": "中度污染",
    "heavy": "重度污染",
    "serious": "严重污染",
    "extreme": "超严重污染",
    "unknown": "未知",
}


def aqi_range(score):
    if score <= 50:
        air_pollution = "fantastic"
    elif score <= 100:
        air_pollution = "good"
    elif score <= 150:
        air_pollution = "moderate"
    elif score <= 200:
        air_pollution = "heavy"
    elif score <= 250:
        air_pollution = "serious"
    elif score == 999:
        air_pollution = "unknown"
    else:
        air_pollution = "extreme"

    return AIR_POLLUTION[air_pollution]

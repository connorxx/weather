import os
from concurrent.futures import ThreadPoolExecutor
from datetime import timedelta

from werkzeug.utils import secure_filename
from flask import Flask, request, Response, json, jsonify, send_from_directory, render_template
import pandas as pd


from service.visual import *
from service.weather_service import *

UPLOAD_FOLDER = "./uploads"
ALLOWED_EXTENSIONS = {"txt", "csv"}

app = Flask(__name__, template_folder='./templates',)


def allowed_file(filename):
    return "." in filename and filename.rsplit(".", 1)[1] in ALLOWED_EXTENSIONS


def get_weather():
    seven_days_weather = []
    hcity = parse_uploads() or hot_city  # 获取上传的城市
    for city in hcity:
        time.sleep(0.3)  # 调用太快会被封掉
        get_t_weather(city, seven_days_weather)

    weather_line(seven_days_weather, hcity)  # 画图

    if len(seven_days_weather):  # 将数据存入csv中
        write2csv(seven_days_weather)
    # return Response(json.dumps({"data": weather_list}, ensure_ascii=False))


@app.route("/weather/upload", methods=["POST"])
def upload_file():
    if request.method == "POST":
        file = request.files["file"]
        if file and allowed_file(file.filename):
            file.save(os.path.join(UPLOAD_FOLDER, "hot_city.txt"))

            get_weather()

            return Response("上传成功，请稍等计算结果")

    return Response("请上传txt结尾的文件")


@app.route("/weather/download", methods=["GET"])
def download_file():
    if request.method == "GET":
        filename = "{}weather.csv".format(
            time.strftime("%Y%m%d", time.localtime(time.time()))
        )
        if os.path.isfile(os.path.join("./downloads", filename)):
            return send_from_directory("./downloads", filename, as_attachment=True)
        else:
            return Response("今日天气未计算完成")


@app.route("/weather/")
def home():
    return render_template("upload.html")


@app.route("/weather/chart")
def chart():
    return render_template("weather.html")


if __name__ == "__main__":
    app.config["UPLOAD_FOLDER"] = UPLOAD_FOLDER
    app.config["MAX_CONTENT_LENGTH"] = 1 * 1024 * 1024  # 最大为1M
    app.config["SEND_FILE_MAX_AGE_DEFAULT"] = timedelta(microseconds=2)
    app.run(debug=False, port=15554)

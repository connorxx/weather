from collections import OrderedDict
import csv
import io
import os
from multiprocessing.dummy import Pool
from pprint import pprint
import time

import xlwt
from flask import make_response
from openpyxl import Workbook
import requests

from utils import json2dict
from utils.aqi import aqi_range
from uploads.hotcity import hot_city
from service.visual import weather_line

WEATHER_URL = "https://api.shenjian.io"
T_WEATHER_URL = "http://t.weather.sojson.com/api/weather/city/"
UPLOAD_CITY_FILE = "uploads/hot_city.txt"
APPID = "f8d5189bf003101edb50832aa0156a43"
del_key = ["date", "week", "sunrise", "sunset"]
city_dict = json2dict.json2dict()
path = os.getcwd()


def get_weather():
    # web.config.debug = False
    # web.header('Content-type', 'application/vnd.ms-excel')  #指定返回的类型
    # web.header('Transfer-Encoding', 'chunked')
    # web.header('Content-Disposition', 'attachment;filename="weather.xls"') #设定用户浏览器显示的保存文件名

    wb = xlwt.Workbook()
    wb.encoding = "gbk"
    ws = wb.add_sheet("1")
    ws.write(0, 1, "123")  # 如果要写中文请使用UNICODE
    # isot = io.StringIO()
    isot = io.BytesIO()
    wb.save(isot)  # 这点很重要，传给save函数的不是保存文件名，而是一个StringIO流
    resp = make_response(isot.getvalue())
    resp.headers["Content-type"] = "application/vnd.ms-excel"
    resp.headers["Transfer-Encoding"] = "chunked"
    resp.headers["Content-Disposition"] = 'attachment;filename="weather.xls"'
    return isot.getvalue().decode("utf8")


def get_shenjian_weather(city_name):
    """通过神箭手API获取数据"""
    weather_list = []
    params = {"appid": APPID, "city_name": city_name}
    res = requests.get(url=WEATHER_URL, params=params, timeout=5).json()

    if res.get("error_code") == 0:
        data = res.get("data")
        forecast7day = data.get("forecast7")

        for everyday in forecast7day:
            day_weather = OrderedDict()
            night_weather = OrderedDict()
            for key, value in everyday.items():
                if key == "date":
                    day_weather[key] = value
                    night_weather[key] = value
                elif key == "day" and len(value) != 0:
                    day_weather["city"] = data.get("city")
                    day_weather["power"] = value[0].get("power")
                    day_weather["direction"] = value[0].get("direction")
                    day_weather["temprature"] = value[0].get("temprature")
                    day_weather["weather"] = value[0].get("weather")
                    day_weather["daytime"] = key
                elif key == "night" and len(value) != 0:
                    night_weather["city"] = data.get("city")
                    night_weather["power"] = value[0].get("power")
                    night_weather["direction"] = value[0].get("direction")
                    night_weather["temprature"] = value[0].get("temprature")
                    night_weather["weather"] = value[0].get("weather")
                    night_weather["daytime"] = key

            warn = everyday.get("warn", 0)
            day_weather["warn"] = (
                everyday.get("warn")[0].get("content", "暂无预警")
                if warn and isinstance(warn, list)
                else "暂无预警"
            )
            night_weather["warn"] = (
                everyday.get("warn")[0].get("content", "暂无预警")
                if warn and isinstance(warn, list)
                else "暂无预警"
            )
            weather_list.append(day_weather)
            weather_list.append(night_weather)
        pprint(weather_list)

    else:
        return res.get("reason")


def get_t_weather(city_name, seven_day_weather: list):
    """通过免费API获取数据"""

    city_code = city_dict.get(city_name, 0)
    if city_code == 0:
        print("failed" + city_name)
        return

    r = requests.get(url=(T_WEATHER_URL + city_code), timeout=5)
    if r.status_code != 200:
        return

    seven_day = r.json().get("data").get("forecast")[:7]
    for everyday in seven_day:
        del everyday["date"]
        del everyday["week"]
        del everyday["sunrise"]
        del everyday["sunset"]
        everyday["city"] = city_name
        everyday["air_pollution"] = aqi_range(everyday.get("aqi", 999))
    print("success " + city_name)
    seven_day_weather.extend(seven_day)


def get_file_name():
    file_name = "downloads/{}weather".format(
        time.strftime("%Y%m%d", time.localtime(time.time()))
    )

    return file_name


def parse_uploads(file=UPLOAD_CITY_FILE):
    with open(os.path.join(path, file), "r", encoding="utf-8") as f:
        hot_city = [line.strip().replace("市", "") for line in f.readlines()]

    return hot_city


def write2excel():
    wb = Workbook()
    sheet = wb.active
    wb.save(file_name)


def write2csv(info):
    file_name = get_file_name()
    with open(os.path.join(path, file_name) + ".csv", "w") as csv_file:
        headers = [k for k in info[1]]
        writer = csv.DictWriter(csv_file, fieldnames=headers)
        writer.writeheader()
        writer.writerows(info)


def execute(tasks):
    pool = Pool(2)
    pool.map(get_weather, tasks)
    pool.close()
    pool.join()


# if __name__ == "__main__":
#     cities = ['佛山', '西安', '苏州']
#
#     for city in cities:
#         time.sleep(0.5)
#         get_t_weather(city)
#
#     weather_line(weather_list, cities)
#
#     write2csv(weather_list)

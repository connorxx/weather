import re

# 导入柱状图-Bar
from pyecharts.charts import Bar
from pyecharts.charts import Line
import pyecharts.options as opts
from pyecharts.faker import Faker

CRITICAL_TEMPERATURE = 5
bar = Bar()
line = Line()
high_temperature = []


def weather_line(weather_7days, cities):
    """将天气的最高温进行绘图"""
    city_weather = {}
    forecast7day = list({"".join(i.get("ymd").split("-")) for i in weather_7days})
    line.add_xaxis(sorted(forecast7day))
    for city in cities:
        city_weather[city] = [
            re.search(r"\d+", i.get("high")).group()
            for i in filter(lambda x: x.get("city") == city, weather_7days)
        ]
    for key, value in city_weather.items():
        if value:
            above_critical_tem = int(max(value)) - int(min(value)) > CRITICAL_TEMPERATURE
            is_select = True if above_critical_tem else False
            line.add_yaxis(key, value, is_smooth=True, is_selected=is_select)

    line.render(path="./templates/weather.html")


def line_base() -> Line:
    chooses = Faker.choose()
    v1 = Faker.values()
    v2 = Faker.values()
    c = (
        Line()
        .add_xaxis([("day" + str(i)) for i in range(5)])
        .add_yaxis("苏州", v1, is_smooth=True)
        .add_yaxis("上海", v2, is_smooth=True)
        .set_global_opts(title_opts=opts.TitleOpts(title="温度变化"))
    )
    return c


if __name__ == "__main__":
    c = line_base()
    c.render()
